package com.example.actor.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "timmet")
public class Timmet {


	  @Id
	  @Column(name="metst")
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private String metst;
	  @Column(name="metend", nullable=false)
	  private String metend;
	  @Column(name="dyupd", nullable=false)
	  private String dyupd;
	  @Column(name="usrset", nullable=false)
	  private String usrset;


	  public String getMetst() {
		return metst;
	}


	public void setMetst(String metst) {
		this.metst = metst;
	}


	public String getMetend() {
		return metend;
	}


	public void setMetend(String metend) {
		this.metend = metend;
	}


	public String getDyupd() {
		return dyupd;
	}


	public void setDyupd(String dyupd) {
		this.dyupd = dyupd;
	}


	public String getUsrset() {
		return usrset;
	}


	public void setUsrset(String usrset) {
		this.usrset = usrset;
	}


	@Override
	  public String toString() {
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	  }

}