package com.example.actor.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "sendbas")
public class SendAdd {

	@Id
	  @Column(name="memcod")
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private String memcod;
	  @Column(name="memnam")
	  private String memnam;
	  @Column(name="countrynmfrm")
	  private String countrynmfrm;
	  @Column(name="zipcod")
	  private String zipcod;
	  @Column(name="addr")
	  private String addr;
	  @Column(name="tele1")
	  private String tele1;
	  @Column(name="tele2")
	  private String tele2;
	  @Column(name="tele3")
	  private String tele3;




  public String getMemcod() {
		return memcod;
	}




	public void setMemcod(String memcod) {
		this.memcod = memcod;
	}




	public String getMemnam() {
		return memnam;
	}




	public void setMemnam(String memnam) {
		this.memnam = memnam;
	}




	public String getCountrynmfrm() {
		return countrynmfrm;
	}




	public void setCountrynmfrm(String countrynmfrm) {
		this.countrynmfrm = countrynmfrm;
	}




	public String getZipcod() {
		return zipcod;
	}




	public void setZipcod(String zipcod) {
		this.zipcod = zipcod;
	}




	public String getAddr() {
		return addr;
	}




	public void setAddr(String addr) {
		this.addr = addr;
	}




	public String getTele1() {
		return tele1;
	}




	public void setTele1(String tele1) {
		this.tele1 = tele1;
	}




	public String getTele2() {
		return tele2;
	}




	public void setTele2(String tele2) {
		this.tele2 = tele2;
	}




	public String getTele3() {
		return tele3;
	}




	public void setTele3(String tele3) {
		this.tele3 = tele3;
	}




@Override
  public String toString() {
      return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }


}
