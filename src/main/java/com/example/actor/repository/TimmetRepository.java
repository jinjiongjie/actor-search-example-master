package com.example.actor.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Siva
 *
 */
@Repository
public class TimmetRepository
{
	@Autowired
	private JdbcTemplate jdbcTemplate;


	@Transactional(readOnly=true)
	public List<Timmet> findTimmetAllValue() {
		return jdbcTemplate.query("select * from timmet", new TimmetRowMapper());
	}

	@Transactional(readOnly=true)
	public News findByNewsId(int id) {
		return jdbcTemplate.queryForObject("select * from news where id=?", new Object[]{id}, new NewsRowMapper());
	}

	public News saveAndFlush(final News news) {
		final String sql = "insert into news(countrycod,doc) values(?,?)";

		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {

                @Override
                public PreparedStatement createPreparedStatement(Connection connection)
                        throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, news.getCountrycod());
                    ps.setString(2, news.getDoc());
                    return ps;
                }
            }, holder);

		int newNewsId = holder.getKey().intValue();
		news.setId(newNewsId);
		return news;
	}

}

class TimmetRowMapper implements RowMapper<Timmet>
{

	@Override
	public Timmet mapRow(ResultSet rs,int intnum) throws SQLException {
		Timmet timmet = new Timmet();
		timmet.setMetst(rs.getString("metst"));
		timmet.setMetend(rs.getString("metend"));
		timmet.setDyupd(rs.getString("dyupd"));
		timmet.setUsrset(rs.getString("usrset"));

		return timmet;
	}

}


