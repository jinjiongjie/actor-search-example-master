package com.example.actor.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "news")
public class News {


	  @Id
	  @Column(name="id")
	  @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private Integer id;
	  @Column(name="countrycod", nullable=false)
	  private String countrycod;
	  @Column(name="doc", nullable=false)
	  private String doc;


	  public Integer getId() {
		    return id;
		  }
	  public void setId(Integer id) {
	    this.id = id;
	  }
	  public String getCountrycod() {
	    return countrycod;
	  }
	  public void setCountrycod(String countrycod) {
	    this.countrycod = countrycod;
	  }

	  public String getDoc() {
		    return doc;
		  }
	  public void setDoc(String doc) {
	    this.doc = doc;
	  }

	  @Override
	  public String toString() {
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	  }

}