package com.example.actor.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SendbasRepository  {

	@Autowired
	private JdbcTemplate jdbcTemplate;



	@Transactional(readOnly=true)
	public Sendbas findBySendbasMemcod(String memcod) {
		return jdbcTemplate.queryForObject("select * from sendbas where memcod=?", new Object[]{memcod}, new SendbasRowMapper());
	}


	class SendbasRowMapper implements RowMapper<Sendbas>
	{

		@Override
		public Sendbas mapRow(ResultSet rs, int rowNum) throws SQLException {
			Sendbas sendbas = new Sendbas();
			sendbas.setMemcod(rs.getString("memcod"));
			sendbas.setCountryid(rs.getString("countryid"));
			sendbas.setZipcod(rs.getString("zipcod"));
			sendbas.setAddrss1(rs.getString("addrss1"));
			sendbas.setAddrss2(rs.getString("addrss2"));
			sendbas.setAddrss3(rs.getString("addrss3"));
			sendbas.setAddrss4(rs.getString("addrss4"));
			sendbas.setAddrss5(rs.getString("addrss5"));
			sendbas.setTele1(rs.getString("tele1"));
			sendbas.setTele2(rs.getString("tele2"));
			sendbas.setTele3(rs.getString("tele3"));
			sendbas.setFre01(rs.getString("fre01"));
			sendbas.setFre02(rs.getString("fre02"));
			sendbas.setFre03(rs.getString("fre03"));
			sendbas.setFre04(rs.getString("fre04"));
			sendbas.setFre05(rs.getString("fre05"));
			sendbas.setFre06(rs.getString("fre06"));
			sendbas.setFre07(rs.getString("fre07"));
			sendbas.setFre08(rs.getString("fre08"));
			sendbas.setFre09(rs.getString("fre09"));
			sendbas.setFre10(rs.getString("fre10"));
			sendbas.setDyupd(rs.getString("dyupd"));
			sendbas.setUsrupd(rs.getString("usrupd"));
			sendbas.setMemo(rs.getString("memo"));
			sendbas.setDelflg(rs.getString("delflg"));
			return sendbas;
		}

	}

}