package com.example.actor.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
//@Table(name = "membas")
public class Membas {


	@Id
	@Column(name="memcod")
	private String  memcod;
	@Column(name="regidat")
	private String  regidat;
	@Column(name="passwrd")
	private String  passwrd;
	@Column(name="firstnmkj")
	private String  firstnmkj;
	@Column(name="secondnmkj")
	private String  secondnmkj;
	@Column(name="firstnmrm")
	private String  firstnmrm;
	@Column(name="secondnmrm")
	private String  secondnmrm;
	@Column(name="firstnmkn")
	private String  firstnmkn;
	@Column(name="secondnmkn")
	private String  secondnmkn;
	@Column(name="sexflg")
	private String  sexflg;
	@Column(name="birthday")
	private String  birthday;
	@Column(name="companynm")
	private String  companynm;
	@Column(name="emailadd")
	private String  emailadd;
	@Column(name="emailmov")
	private String  emailmov;
	@Column(name="countryid")
	private String  countryid;
	@Column(name="zipcod")
	private String  zipcod;
	@Column(name="addrss1")
	private String  addrss1;
	@Column(name="addrss2")
	private String  addrss2;
	@Column(name="addrss3")
	private String  addrss3;
	@Column(name="addrss4")
	private String  addrss4;
	@Column(name="addrss5")
	private String  addrss5;
	@Column(name="tele1")
	private String  tele1;
	@Column(name="tele2")
	private String  tele2;
	@Column(name="tele3")
	private String  tele3;
	@Column(name="fre01")
	private String  fre01;
	@Column(name="fre02")
	private String  fre02;
	@Column(name="fre03")
	private String  fre03;
	@Column(name="fre04")
	private String  fre04;
	@Column(name="fre05")
	private String  fre05;
	@Column(name="fre06")
	private String  fre06;
	@Column(name="fre07")
	private String  fre07;
	@Column(name="fre08")
	private String  fre08;
	@Column(name="fre09")
	private String  fre09;
	@Column(name="fre10")
	private String  fre10;
	@Column(name="dyupd")
	private String  dyupd;
	@Column(name="usrupd")
	private String  usrupd;
	@Column(name="memo")
	private String  memo;
	@Column(name="delflg")
	private String  delflg;

	public String getMemcod() {
		return memcod;
	}


	public void setMemcod(String memcod) {
		this.memcod = memcod;
	}


	public String getRegidat() {
		return regidat;
	}


	public void setRegidat(String regidat) {
		this.regidat = regidat;
	}


	public String getPasswrd() {
		return passwrd;
	}


	public void setPasswrd(String passwrd) {
		this.passwrd = passwrd;
	}


	public String getFirstnmkj() {
		return firstnmkj;
	}


	public void setFirstnmkj(String firstnmkj) {
		this.firstnmkj = firstnmkj;
	}


	public String getSecondnmkj() {
		return secondnmkj;
	}


	public void setSecondnmkj(String secondnmkj) {
		this.secondnmkj = secondnmkj;
	}


	public String getFirstnmrm() {
		return firstnmrm;
	}


	public void setFirstnmrm(String firstnmrm) {
		this.firstnmrm = firstnmrm;
	}


	public String getSecondnmrm() {
		return secondnmrm;
	}


	public void setSecondnmrm(String secondnmrm) {
		this.secondnmrm = secondnmrm;
	}


	public String getFirstnmkn() {
		return firstnmkn;
	}


	public void setFirstnmkn(String firstnmkn) {
		this.firstnmkn = firstnmkn;
	}


	public String getSecondnmkn() {
		return secondnmkn;
	}


	public void setSecondnmkn(String secondnmkn) {
		this.secondnmkn = secondnmkn;
	}


	public String getSexflg() {
		return sexflg;
	}


	public void setSexflg(String sexflg) {
		this.sexflg = sexflg;
	}


	public String getBirthday() {
		return birthday;
	}


	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}


	public String getCompanynm() {
		return companynm;
	}


	public void setCompanynm(String companynm) {
		this.companynm = companynm;
	}


	public String getEmailadd() {
		return emailadd;
	}


	public void setEmailadd(String emailadd) {
		this.emailadd = emailadd;
	}


	public String getEmailmov() {
		return emailmov;
	}


	public void setEmailmov(String emailmov) {
		this.emailmov = emailmov;
	}


	public String getCountryid() {
		return countryid;
	}


	public void setCountryid(String countryid) {
		this.countryid = countryid;
	}


	public String getZipcod() {
		return zipcod;
	}


	public void setZipcod(String zipcod) {
		this.zipcod = zipcod;
	}


	public String getAddrss1() {
		return addrss1;
	}


	public void setAddrss1(String addrss1) {
		this.addrss1 = addrss1;
	}


	public String getAddrss2() {
		return addrss2;
	}


	public void setAddrss2(String addrss2) {
		this.addrss2 = addrss2;
	}


	public String getAddrss3() {
		return addrss3;
	}


	public void setAddrss3(String addrss3) {
		this.addrss3 = addrss3;
	}


	public String getAddrss4() {
		return addrss4;
	}


	public void setAddrss4(String addrss4) {
		this.addrss4 = addrss4;
	}


	public String getAddrss5() {
		return addrss5;
	}


	public void setAddrss5(String addrss5) {
		this.addrss5 = addrss5;
	}


	public String getTele1() {
		return tele1;
	}


	public void setTele1(String tele1) {
		this.tele1 = tele1;
	}


	public String getTele2() {
		return tele2;
	}


	public void setTele2(String tele2) {
		this.tele2 = tele2;
	}


	public String getTele3() {
		return tele3;
	}


	public void setTele3(String tele3) {
		this.tele3 = tele3;
	}


	public String getFre01() {
		return fre01;
	}


	public void setFre01(String fre01) {
		this.fre01 = fre01;
	}


	public String getFre02() {
		return fre02;
	}


	public void setFre02(String fre02) {
		this.fre02 = fre02;
	}


	public String getFre03() {
		return fre03;
	}


	public void setFre03(String fre03) {
		this.fre03 = fre03;
	}


	public String getFre04() {
		return fre04;
	}


	public void setFre04(String fre04) {
		this.fre04 = fre04;
	}


	public String getFre05() {
		return fre05;
	}


	public void setFre05(String fre05) {
		this.fre05 = fre05;
	}


	public String getFre06() {
		return fre06;
	}


	public void setFre06(String fre06) {
		this.fre06 = fre06;
	}


	public String getFre07() {
		return fre07;
	}


	public void setFre07(String fre07) {
		this.fre07 = fre07;
	}


	public String getFre08() {
		return fre08;
	}


	public void setFre08(String fre08) {
		this.fre08 = fre08;
	}


	public String getFre09() {
		return fre09;
	}


	public void setFre09(String fre09) {
		this.fre09 = fre09;
	}


	public String getFre10() {
		return fre10;
	}


	public void setFre10(String fre10) {
		this.fre10 = fre10;
	}


	public String getDyupd() {
		return dyupd;
	}


	public void setDyupd(String dyupd) {
		this.dyupd = dyupd;
	}


	public String getUsrupd() {
		return usrupd;
	}


	public void setUsrupd(String usrupd) {
		this.usrupd = usrupd;
	}


	public String getMemo() {
		return memo;
	}


	public void setMemo(String memo) {
		this.memo = memo;
	}


	public String getDelflg() {
		return delflg;
	}


	public void setDelflg(String delflg) {
		this.delflg = delflg;
	}



	  @Override
	  public String toString() {
	      return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	  }

}