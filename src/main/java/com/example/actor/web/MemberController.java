package com.example.actor.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.actor.repository.Membas;
import com.example.actor.repository.MemberRepository;
import com.example.actor.repository.Prefecture;
import com.example.actor.repository.PrefectureRepository;
import com.example.actor.service.MemberService;


@Controller
public class MemberController {
  final static Logger logger = LoggerFactory.getLogger(MemberController.class);

  @Autowired(required=false)
  MemberRepository membRepository;

  @Autowired
  MemberService memService;

  @Autowired
  PrefectureRepository prefectureRepository;

  @Autowired
  MessageSource msg;

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    sdf.setLenient(false);
    binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
  }

  @RequestMapping(value = "/member", method = RequestMethod.GET)
  public String index(Model model) {
    logger.debug("Member + index");
    //Membas mem = memService.findMemOne(new String("00000227"));
    List<Membas> list = new ArrayList<Membas>();
    list = memService.findAllMem();
    //list.add(mem);

    if (CollectionUtils.isEmpty(list)) {
      String message = msg.getMessage("member.list.empty", null, Locale.JAPAN);
      model.addAttribute("emptyMessage", message);
    }else {
	    model.addAttribute("list", list);
	    //modelDump(model, "index");
    }
    return "Member/index";
  }

  @RequestMapping(value = "/member/create", method = RequestMethod.GET)
  public String create(MemberForm form, Model model) {
    logger.debug("Member + create");
    List<Prefecture> pref = prefectureRepository.findAll();
    model.addAttribute("pref", pref);
    //modelDump(model, "create");
    return "Member/create";
  }

  @RequestMapping(value = "/member/save", method = RequestMethod.POST)
  public String save(@Validated @ModelAttribute MemberForm form, BindingResult result, Model model) {
    logger.debug("Member + save");
    if (result.hasErrors()) {
      String message = msg.getMessage("member.validation.error", null, Locale.JAPAN);
      model.addAttribute("errorMessage", message);
      return create(form, model);
    }
    Membas memb = convert(form);
    logger.debug("actor:{}", memb.toString());
    memb = membRepository.saveAndFlush(memb);
    //modelDump(model, "save");
    return "redirect:/member/" ;
  }

  /**
   * convert form to model.
   */
  private Membas convert(MemberForm form) {
	Membas memb = new Membas();
    memb.setMemcod(form.getMemcod());
    memb.setRegidat(form.getRegidat());
    memb.setFirstnmkj(form.getFirstnmkj());
    memb.setSecondnmkj(form.getSecondnmkj());
    memb.setFirstnmrm(form.getFirstnmrm());
    memb.setSecondnmrm(form.getSecondnmrm());
    memb.setFirstnmkn(form.getFirstnmkn());
    memb.setSecondnmkn(form.getSecondnmkn());

    Date date = null;
    try {
        date = toDate(form.getBirthday(), "yyyy-MM-dd");
    } catch (ParseException e) {
        System.out.println(e);
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    memb.setBirthday(sdf.format(date));

    memb.setCompanynm(form.getCompanynm());
    memb.setEmailadd(form.getEmailadd());
    memb.setPasswrd(form.getPasswrd());
    memb.setZipcod(form.getZipcod());
    memb.setAddrss1(form.getAddrss1());
    memb.setAddrss2(form.getAddrss2());
    memb.setAddrss3(form.getAddrss3());
    memb.setAddrss4(form.getAddrss4());
    memb.setTele1(form.getTele1());
    memb.setTele2(form.getTele2());
    memb.setTele3(form.getTele3());


    return memb;
  }

  /**
   * for debug.
   */
  private void modelDump(Model model, String m) {
    logger.debug(" ");
    logger.debug("Model:{}", m);
    Map<String, Object> mm = model.asMap();
    for (Entry<String, Object> entry : mm.entrySet()) {
      logger.debug("key:{}, value:{}", entry.getKey(), entry.getValue().toString());
    }
  }
  public static Date toDate(String str, String format) throws ParseException {
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      Date date = sdf.parse(str);
      return date;
  }

}
