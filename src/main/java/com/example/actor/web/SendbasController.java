package com.example.actor.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.actor.repository.Prefecture;
import com.example.actor.repository.PrefectureRepository;
import com.example.actor.repository.SendAdd;
import com.example.actor.repository.Sendbas;
import com.example.actor.repository.SendbasRepository;
import com.example.actor.service.MemberService;

@Controller
public class SendbasController {
  final static Logger logger = LoggerFactory.getLogger(SendbasController.class);

  @Autowired
  SendbasRepository sendbasRepository;

  @Autowired
  PrefectureRepository prefectureRepository;

  @Autowired
  MemberService sendService;

  @Autowired
  MessageSource msg;

  //初期化
  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    sdf.setLenient(false);
    binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
  }
//一覧
  @RequestMapping(value = "/sendbas", method = RequestMethod.GET)
  public String index(Model model) {
	  logger.debug("Sendbas + index");
	  //List<Sendbas> list = sendbasRepository.findAll();
	  List<SendAdd> list = sendService.findAllMemBySend();
	 // logger.debug(list.toString());
	  if (CollectionUtils.isEmpty(list)) {
		  String message = msg.getMessage("sendbas.list.empty", null, Locale.JAPAN);
		  model.addAttribute("emptyMessage", message);
	  }
	  model.addAttribute("list", list);
	 //modelDump(model, "index");
	  return "Sendbas/index";
  }
//詳細
  @RequestMapping(value = "/sendbas/{memcod}", method = RequestMethod.GET)
  public ModelAndView detail(@PathVariable String memcod) {
    logger.debug("Senbas + detail");
    ModelAndView mv = new ModelAndView();
    mv.setViewName("Sendbas/detail");
    Sendbas sendbas = sendbasRepository.findBySendbasMemcod(memcod);
    mv.addObject("sendbas", sendbas);
    return mv;
  }
//検索
  @RequestMapping(value = "/sendbas/search", method = RequestMethod.GET)
  public ModelAndView search(@RequestParam String keyword) {
    logger.debug("Sendbas + search");
    ModelAndView mv = new ModelAndView();
    mv.setViewName("Actor/index");
    if (StringUtils.isNotEmpty(keyword)) {
      List<Sendbas> list = null ; //sendbasRepository.findSendbas(keyword);
      if (CollectionUtils.isEmpty(list)) {
        String message = msg.getMessage("Sendbas.list.empty", null, Locale.JAPAN);
        mv.addObject("emptyMessage", message);
      }
      mv.addObject("list", list);
    }
    return mv;
  }
//新規
  @RequestMapping(value = "/sendbas/create", method = RequestMethod.GET)
  public String create(SendbasForm form, Model model) {
    logger.debug("Sendbas + create");
    List<Prefecture> pref = prefectureRepository.findAll();
    model.addAttribute("pref", pref);
    //modelDump(model, "create");
    return "Sendbas/create";
  }
//保存
  @RequestMapping(value = "/sendbas/save", method = RequestMethod.POST)
  public String save(@Validated @ModelAttribute SendbasForm form, BindingResult result, Model model) {
    logger.debug("Sendbas + save");
    if (result.hasErrors()) {
      String message = msg.getMessage("sendbas.validation.error", null, Locale.JAPAN);
      model.addAttribute("errorMessage", message);
      return create(form, model);
    }
    //画面の値はテーブルビーンへ伝送
    Sendbas sendbas = convert(form);
    logger.debug("sendbas:{}", sendbas.toString());
    //テーブルへ保存
    sendbas = null ;//sendbasRepository.saveAndFlush(sendbas);
    //modelDump(model, "save");
    return "redirect:/sendbas/" + sendbas.getMemcod().toString();
  }
//削除
  @RequestMapping(value = "/memcod/delete/{memcod}", method = RequestMethod.GET)
  public String delete(@PathVariable String memcod, RedirectAttributes attributes, Model model) {
    logger.debug("Sendbas + delete");
    // sendbasRepository.delete(memcod);
    attributes.addFlashAttribute("deleteMessage", "delete MEMCOD:" + memcod);
    return "redirect:/sendbas";
  }


  /**
   * convert form to model.
   */
  private Sendbas convert(SendbasForm form) {
	Sendbas sendbas = new Sendbas();
	sendbas.Setcountryid(form.getCountryid());
    if (StringUtils.isNotEmpty(form.getZipcod())) {
    	sendbas.setZipcod(form.getZipcod());
    }
    return sendbas;
  }

  /**
   * for debug.
   */
  private void modelDump(Model model, String m) {
    logger.debug(" ");
    logger.debug("Model:{}", m);
    Map<String, Object> mm = model.asMap();
    for (Entry<String, Object> entry : mm.entrySet()) {
      logger.debug("key:{}, value:{}", entry.getKey(), entry.getValue().toString());
    }
  }

}
