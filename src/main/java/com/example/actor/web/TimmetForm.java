package com.example.actor.web;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TimmetForm implements Serializable {

  private static final long serialVersionUID = 1330043957072942389L;


  private String metst;
  private String metend;
  private String dyupd;
  private String usrset;


public String getMetst() {
	return metst;
}


public void setMetst(String metst) {
	this.metst = metst;
}


public String getMetend() {
	return metend;
}


public void setMetend(String metend) {
	this.metend = metend;
}


public String getDyupd() {
	return dyupd;
}


public void setDyupd(String dyupd) {
	this.dyupd = dyupd;
}


public String getUsrset() {
	return usrset;
}


public void setUsrset(String usrset) {
	this.usrset = usrset;
}


@Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }

}
