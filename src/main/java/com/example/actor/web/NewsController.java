package com.example.actor.web;

import java.lang.Character.UnicodeBlock;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.actor.repository.News;
import com.example.actor.repository.NewsRepository;
import com.example.actor.service.MemberService;

@Controller
public class NewsController {
  final static Logger logger = LoggerFactory.getLogger(NewsController.class);

  @Autowired
  NewsRepository newsRepository;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  MemberService memService;

  @Autowired
  EntityManager em;

  @Autowired
  MessageSource msg;

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    sdf.setLenient(false);
    binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
  }

  @RequestMapping(value = "/news", method = RequestMethod.GET)
  public String index(Model model) {
    logger.debug("News + index");

    List<News> list = newsRepository.findNewsAllValue();
    if (CollectionUtils.isEmpty(list)) {
      String message = msg.getMessage("news.list.empty", null, Locale.JAPAN);
      model.addAttribute("emptyMessage", message);
    }
    model.addAttribute("list", list);
    modelDump(model, "index");
    return "News/index";
  }

  @RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
  public ModelAndView detail(@PathVariable Integer id) {
    logger.debug("News + detail");
    ModelAndView mv = new ModelAndView();
    mv.setViewName("News/detail");
    //News news = newsRepository.findOne(id);
//    News p_news = new News();
//    p_news.setId(id);
    News news = newsRepository.findByNewsId(id);

    mv.addObject("news", news);
    return mv;
  }
  @RequestMapping(value = "/news/create", method = RequestMethod.GET)
  public String create(NewsForm form, Model model) {
    logger.debug("News + create");
    modelDump(model, "create");
    return "News/create";
  }


  @RequestMapping(value = "/news/updat/{id}", method = RequestMethod.GET)
  public ModelAndView updat(NewsForm form, Model model,@PathVariable Integer id) {
    logger.debug("News + updat");
    ModelAndView mv = new ModelAndView();
    mv.setViewName("News/updat");
    News news = newsRepository.findByNewsId(id);

    modelDump(model, "updat");

    mv.addObject("newsForm", news);
    return mv;

  }

  @RequestMapping(value = "/news/updat", method = RequestMethod.POST)
  public String updat(@Validated @ModelAttribute NewsForm form, BindingResult result, Model model) {
    logger.debug("News + update");
    if (result.hasErrors()) {
      String message = msg.getMessage("news.validation.error", null, Locale.JAPAN);
      model.addAttribute("errorMessage", message);
      return create(form, model);
    }
    News news = convertToUpdat(form);
    logger.debug("newsnewsnewsnewsnews:{}", news.toString());
    int ret = memService.updat(news);
    modelDump(model, "save");
    return "redirect:/news/" + news.getId().toString();
  }


  @RequestMapping(value = "/news/save", method = RequestMethod.POST)
  public String save(@Validated @ModelAttribute NewsForm form, BindingResult result, Model model) {
    logger.debug("News + save");
    if (result.hasErrors()) {
      String message = msg.getMessage("news.validation.error", null, Locale.JAPAN);
      model.addAttribute("errorMessage", message);
      return create(form, model);
    }

    boolean isIncl = false;
    char[] ch = form.getCountrycod().toCharArray();
    for (char c : ch) {
         if (UnicodeBlock.of( c ) == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS ) {
        	 	isIncl = true;
         }
    }
    if(isIncl) {
        String message = msg.getMessage("news.validation.error", null, Locale.JAPAN);
        model.addAttribute("errorMessage", message);
        return create(form, model);

    }
    News news = convert(form);
    logger.debug("news:{}", news.toString());
    news = newsRepository.saveAndFlush(news);
    modelDump(model, "save");
    return "redirect:/news/" + news.getId().toString();
  }

  /**
   * convert form to model.
   */
  private News convertToUpdat(NewsForm form) {
    News news = new News();
    news.setId(form.getId());
    news.setCountrycod(form.getCountrycod());
    news.setDoc(form.getDoc());

    return news;
  }

  /**
   * convert form to model.
   */
  private News convert(NewsForm form) {
    News news = new News();
    news.setCountrycod(form.getCountrycod());
    news.setDoc(form.getDoc());

    return news;
  }

  /**
   * for debug.
   */
  private void modelDump(Model model, String m) {
    logger.debug(" ");
    logger.debug("Model:{}", m);
    Map<String, Object> mm = model.asMap();
    for (Entry<String, Object> entry : mm.entrySet()) {
      logger.debug("key:{}, value:{}", entry.getKey(), entry.getValue().toString());
    }
  }

//  private List<String> getUserRoles(int id) {
//
//	     List<String> roles = this.jdbcTemplate.query("SELECT * FROM news where id=?;",
//	         new Object[]{id},new ResultSetExtractor<List<String>>() {
//	             @Override
//	             public List<String> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
//	                 List<String> roles = new ArrayList<>();
//	                 while (resultSet.next()) {
//	                     roles.add(resultSet.getString("authority"));
//	                 }
//	                 return roles;
//	             }
//	         });
//	     return roles;
//	 }
}
