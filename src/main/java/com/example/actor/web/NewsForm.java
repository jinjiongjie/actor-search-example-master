package com.example.actor.web;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class NewsForm implements Serializable {

  private static final long serialVersionUID = 1330043957072942389L;


  private Integer id;

  @NotNull
  @Size(min=1, max=2)
  private String countrycod;
  @Size(min=1, max=30)
  private String doc;

  public String getCountrycod() {
    return countrycod;
  }
  public void setCountrycod(String countrycod) {
    this.countrycod = countrycod;
  }
  public String getDoc() {
    return doc;
  }
  public void setDoc(String doc) {
    this.doc = doc;
  }

  public Integer getId() {
	return id;
  }

  public void setId(Integer id) {
	this.id = id;
  }
@Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }

}
