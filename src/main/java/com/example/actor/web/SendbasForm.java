package com.example.actor.web;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class SendbasForm implements Serializable {

  private static final long serialVersionUID = 1330043957072942381L;

  @NotNull
  @Size( max=16)
  private String memcod;

  @NotNull
  @Size(max=4)
  private String countryid;

  @NotNull
  @Size(max=10)
  private String zipcod;

  @NotNull
  @Size(max=100)
  private String addrss1;

  @NotNull
  @Size(max=100)
  private String addrss2;

  @NotNull
  @Size(max=100)
  private String addrss3;

  @NotNull
  @Size(max=100)
  private String addrss5;

  @NotNull
  @Size(max=20)
  private String tele1;

  @NotNull
  @Size(max=20)
  private String tele2;

  @NotNull
  @Size(max=20)
  private String tele3;

  @NotNull
  @Size(max=50)
  private String fre01;

  @NotNull
  @Size(max=50)
  private String fre02;

  @NotNull
  @Size(max=50)
  private String fre03;

  @NotNull
  @Size(max=50)
  private String fre04;

  @NotNull
  @Size(max=50)
  private String fre05;

  @NotNull
  @Size(max=50)
  private String fre06;

  @NotNull
  @Size(max=50)
  private String fre07;

  @NotNull
  @Size(max=50)
  private String fre08;

  @NotNull
  @Size(max=50)
  private String fre09;

  @NotNull
  @Size(max=50)
  private String fre10;

  @NotNull
  @Size(max=20)
  private String dyupd;

  @NotNull
  @Size(max=20)
  private String usrupd;

  @NotNull
  @Size(max=100)
  private String memo;

  @NotNull
  @Size(max=1)
  private String delflg;





  public String getMemcod() {
	return memcod;
}


public void setMemcod(String memcod) {
	this.memcod = memcod;
}


public String getCountryid() {
	return countryid;
}


public void setCountryid(String countryid) {
	this.countryid = countryid;
}


public String getZipcod() {
	return zipcod;
}


public void setZipcod(String zipcod) {
	this.zipcod = zipcod;
}


public String getAddrss1() {
	return addrss1;
}


public void setAddrss1(String addrss1) {
	this.addrss1 = addrss1;
}


public String getAddrss2() {
	return addrss2;
}


public void setAddrss2(String addrss2) {
	this.addrss2 = addrss2;
}


public String getAddrss3() {
	return addrss3;
}


public void setAddrss3(String addrss3) {
	this.addrss3 = addrss3;
}


public String getAddrss5() {
	return addrss5;
}


public void setAddrss5(String addrss5) {
	this.addrss5 = addrss5;
}


public String getTele1() {
	return tele1;
}


public void setTele1(String tele1) {
	this.tele1 = tele1;
}


public String getTele2() {
	return tele2;
}


public void setTele2(String tele2) {
	this.tele2 = tele2;
}


public String getTele3() {
	return tele3;
}


public void setTele3(String tele3) {
	this.tele3 = tele3;
}


public String getFre01() {
	return fre01;
}


public void setFre01(String fre01) {
	this.fre01 = fre01;
}


public String getFre02() {
	return fre02;
}


public void setFre02(String fre02) {
	this.fre02 = fre02;
}


public String getFre03() {
	return fre03;
}


public void setFre03(String fre03) {
	this.fre03 = fre03;
}


public String getFre04() {
	return fre04;
}


public void setFre04(String fre04) {
	this.fre04 = fre04;
}


public String getFre05() {
	return fre05;
}


public void setFre05(String fre05) {
	this.fre05 = fre05;
}


public String getFre06() {
	return fre06;
}


public void setFre06(String fre06) {
	this.fre06 = fre06;
}


public String getFre07() {
	return fre07;
}


public void setFre07(String fre07) {
	this.fre07 = fre07;
}


public String getFre08() {
	return fre08;
}


public void setFre08(String fre08) {
	this.fre08 = fre08;
}


public String getFre09() {
	return fre09;
}


public void setFre09(String fre09) {
	this.fre09 = fre09;
}


public String getFre10() {
	return fre10;
}


public void setFre10(String fre10) {
	this.fre10 = fre10;
}


public String getDyupd() {
	return dyupd;
}


public void setDyupd(String dyupd) {
	this.dyupd = dyupd;
}


public String getUsrupd() {
	return usrupd;
}


public void setUsrupd(String usrupd) {
	this.usrupd = usrupd;
}


public String getMemo() {
	return memo;
}


public void setMemo(String memo) {
	this.memo = memo;
}


public String getDelflg() {
	return delflg;
}


public void setDelflg(String delflg) {
	this.delflg = delflg;
}


  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
  }

}
