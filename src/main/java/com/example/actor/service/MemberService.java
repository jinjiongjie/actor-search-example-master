package com.example.actor.service;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.actor.repository.Membas;
import com.example.actor.repository.MemberRepository;
import com.example.actor.repository.News;
import com.example.actor.repository.SendAdd;

@Service
@Transactional
public class MemberService {
    @Autowired
    MemberRepository memRepository;

//    public Membas save(Membas customer){
//        return memRepository.save(customer);
//    }
    public List<Membas> findAll(){
        return memRepository.findAll();
    }

    public Membas findOne(Integer id){
        return memRepository.findOne(id);
    }
    public Membas create(Membas custmer){
        return memRepository.save(custmer);
    }
    public Membas update(Membas customer){
        return memRepository.save(customer);
    }
//    public void delete(Integer id){
//    	memRepository.delete(id);
//    }

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public List<Membas> findAllMem() {

        return jdbcTemplate.query(
                "SELECT * FROM membas ORDER BY memcod",
                new BeanPropertyRowMapper<Membas>(Membas.class));
    }

    public List<SendAdd> findAllMemBySend() {

        return jdbcTemplate.query(
                "select\r\n" +
                "  ta.memcod\r\n" +
                ", concat(tb.firstnmkj,tb.secondnmkj) as memnam " +
                "  , td.countrynmfrm\r\n" +
                "  , ta.zipcod\r\n" +
                "  , CONCAT (tc.name , ta.addrss2 , ta.addrss3 , ta.addrss4 , ta.addrss5) as addr \r\n" +
                "  , ta.tele1\r\n" +
                "  , ta.tele2\r\n" +
                "  , ta.tele3\r\n" +
                "  from\r\n" +
                "  sendbas ta \r\n" +
                "  inner join membas tb \r\n" +
                "    on (ta.memcod = tb.memcod) \r\n" +
                "  inner join prefecture tc \r\n" +
                "    on (cast(ta.addrss1 as signed) = tc.id)\r\n" +
                "  inner join countrybas td \r\n" +
                "    on (ta.countryid = td.countryid)\r\n" +
                "order by\r\n" +
                "  ta.memcod",
                new BeanPropertyRowMapper<SendAdd>(SendAdd.class));
    }


    public Membas findMemOne(String memid) {

        SqlParameterSource param = new MapSqlParameterSource().addValue("memid", memid);

        try {
            return null;
//            		jdbcTemplate.query(
//                    "SELECT * FROM membas WHERE memcod = :memid",
//                    param,
//                    new BeanPropertyRowMapper<Membas>(Membas.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
    public Membas save(Membas customer) {

        SqlParameterSource param = new BeanPropertySqlParameterSource(customer);

        if (customer.getMemcod() == null) {

//            SimpleJdbcInsert insert =
//                    new SimpleJdbcInsert((JdbcTemplate) jdbcTemplate.getJdbcOperations())
//                            .withTableName("membas")
//                            .usingGeneratedKeyColumns("memcod");

        	SimpleJdbcInsert insert = null;
            Number key = insert.executeAndReturnKey(param);
            customer.setMemcod(key.toString());
        } else {
            jdbcTemplate.update(
                    "UPDATE customers SET first_name = :firstName, last_name = :lastName, address = :address WHERE id = :id",
                    param);
        }

        return customer;
    }

    public int updat(News customer) {

    	NamedParameterJdbcTemplate jdbctemp = new NamedParameterJdbcTemplate(jdbcTemplate);
    	int ret = jdbctemp.update(
    			"UPDATE news SET countrycod = :countrycod, doc = :doc WHERE id = :id",
    			 new BeanPropertySqlParameterSource(customer));

        return ret;
    }


    public void delete(Integer id) {

        SqlParameterSource param = new MapSqlParameterSource().addValue("id", id);

        jdbcTemplate.update(
                "DELETE FROM customers WHERE id = :id",
                param);
    }

    public List<Membas> findByFirstName(String firstName) {

        SqlParameterSource param = new MapSqlParameterSource().addValue("firstName", "%" + firstName + "%");

        return null;
//        		jdbcTemplate.query(
//                "SELECT * FROM customers WHERE first_name LIKE :firstName ORDER BY id",
//                param,
//                new BeanPropertyRowMapper<Membas>(Membas.class));
    }

    public void deleteAll() {

        jdbcTemplate.update("DELETE FROM customers", new HashMap<>());
    }


}